const TOKEN_KEY = "jwt_token";

const AuthService = {
  saveToken: (token) => {
    localStorage.setItem(TOKEN_KEY, token);
  },

  getToken: () => {
    return localStorage.getItem(TOKEN_KEY);
  },

  deleteToken: () => {
    localStorage.removeItem(TOKEN_KEY);
  },

  isTokenValid: async () => {
    const token = AuthService.getToken();

    if (!token) return false;

    // 這裡假定了一個API端點，你應該根據實際情況調整。
    const response = await fetch("/api/check-token-validity", {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });

    return response.status === 200;
  },
};

export default AuthService;
