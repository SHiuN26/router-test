import { useState, useEffect } from "react";
import { Outlet, Navigate } from "react-router-dom";
import AuthService from "./AuthService";

function PrivateRoute() {
  const [isLoading, setIsLoading] = useState(true);
  const [isAuthenticated, setIsAuthenticated] = useState(false);

  useEffect(() => {
    async function checkTokenValidity() {
      const isValid = await AuthService.isTokenValid();
      setIsAuthenticated(isValid);
      setIsLoading(false);
    }
    checkTokenValidity();
  }, []);

  if (isLoading) {
    return <div>Loading...</div>;
  }

  return isAuthenticated ? <Outlet /> : <Navigate to="/login" />;
}

export default PrivateRoute;
