import React, { useState } from "react";
import AuthService from "./AuthService";

function LoginPage() {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [errorMessage, setErrorMessage] = useState("");

  const handleLogin = async (e) => {
    e.preventDefault();

    // 模擬認證過程。在實際應用中，你可能需要呼叫後端API。
    if (username === "admin" && password === "password") {
      const mockToken = "mocked_jwt_token"; // 通常你會從後端API獲取真正的JWT token。
      AuthService.saveToken(mockToken);
      window.location.href = "/lobby"; // 將用戶導向大廳頁面
    } else {
      setErrorMessage("Invalid credentials. Try admin/password for this demo.");
    }
  };

  return (
    <div>
      <h1>Login Page</h1>
      {errorMessage && <p style={{ color: "red" }}>{errorMessage}</p>}
      <form onSubmit={handleLogin}>
        <div>
          <label>
            Username:
            <input
              type="text"
              value={username}
              onChange={(e) => setUsername(e.target.value)}
            />
          </label>
        </div>
        <div>
          <label>
            Password:
            <input
              type="password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
          </label>
        </div>
        <button type="submit">Login</button>
      </form>
    </div>
  );
}

export default LoginPage;
