import React from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import LoginPage from "./LoginPage";
import LobbyPage from "./LobbyPage";
import PrivateRoute from "./PrivateRoute";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/login" element={<LoginPage />} />
        <Route path="/lobby" element={<PrivateRoute />}>
          <Route index element={<LobbyPage />} />
        </Route>
      </Routes>
    </BrowserRouter>
  );
}

export default App;
