import React, { useState, useEffect } from "react";
import AuthService from "./AuthService";

function LobbyPage() {
  // const [role, setRole] = useState(null);
  // const [error, setError] = useState(null);

  // useEffect(() => {
  //   async function fetchRole() {
  //     const userRole = await AuthService.getUserRole();
  //     if (userRole) {
  //       setRole(userRole);
  //     } else {
  //       setError("Failed to fetch role, please login again.");
  //     }
  //   }

  //   fetchRole();
  // }, []);

  // if (error) {
  //   return <p>{error}</p>; // 或其他通知用戶重新登入的方法
  // }

  return (
    <div>
      <h1>Welcome to the Lobby!</h1>
      <p>This is a restricted area, only accessible when logged in.</p>
      {/* {role && <p>Your role is: {role}</p>} */}
    </div>
  );
}
export default LobbyPage();
